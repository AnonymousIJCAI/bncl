from lib.nvhcf import *
import numpy as np
import tensorflow as tf
import scipy.io
from lib.utils import *

np.random.seed(1)
tf.set_random_seed(1)

def load_data(data_dir):
  data = {}
  print('load data')
  if(data_dir == "data/lastfm/"):
    variables = scipy.io.loadmat(data_dir + "content.mat")
    data["user_x0"] = variables['user_x0']
    data["user_x1"] = variables['user_x1']
    data["item_x0"] = variables['item_x0']
    data["item_x1"] = variables['item_x1']
    data['y'] = variables['y']

  return data


params = Params()
params.n_epochs = 100
params.max_iter = 1

data = load_data("data/lastfm/")
num_factors = 50
model = NVHCF(num_users=1892, num_items=17632, num_factors=num_factors, params=params, 
    input_dim= [17632,1892,1892,11946], dims=[200, 100], n_z=num_factors, activations=['sigmoid', 'sigmoid'], 
    loss_type='cross-entropy', lr=0.001, random_seed=0, print_step=10, verbose=False)
# model.load_model(weight_path="model/pretrain")
model.run(data["user_x0"], data["user_x1"], data["item_x0"], data["item_x1"], data['y'], params)
