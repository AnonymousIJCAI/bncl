import numpy as np
import lib.utils as utils
import tensorflow as tf
import sys
import math
import scipy
import scipy.io
import logging

class Params:
    """Parameters for DMF
    """
    def __init__(self):
        self.max_iter = 10
        self.M = 300

        # for updating W and b
        self.lr = 0.001
        self.batch_size = 64
        self.n_epochs = 10

class NVHCF:
    def __init__(self, num_users, num_items, num_factors, params, input_dim, 
        dims, activations, n_z=50, loss_type='cross-entropy', lr=0.1, 
        wd=1e-4, dropout=0.1, random_seed=0, print_step=50, verbose=True):
        self.m_num_users = num_users
        self.m_num_items = num_items
        self.m_num_factors = num_factors

        self.input_dim = input_dim
        self.dims = dims
        self.activations = activations
        self.lr = lr
        self.params = params
        self.print_step = print_step
        self.verbose = verbose
        self.loss_type = loss_type
        self.n_z = n_z
        self.weights = []
        self.reg_loss = 0

        self.user_x0 = tf.placeholder(tf.float32, [None, self.input_dim[0]], name='user_x0')
        self.user_x1 = tf.placeholder(tf.float32, [None, self.input_dim[1]], name='user_x1')
        self.item_x0 = tf.placeholder(tf.float32, [None, self.input_dim[2]], name='item_x0')
        self.item_x1 = tf.placeholder(tf.float32, [None, self.input_dim[3]], name='item_x1')

        x_recon_user,x_recon_item,pred = self.forward(self.x0,self.x1) 

        # loss
        # reconstruction loss
        if loss_type == 'rmse':
            self.gen_loss = tf.reduce_mean(tf.square(tf.sub(self.user_x0, x_recon_user)) + tf.square(tf.sub(self.item_x0, x_recon_item)) + tf.square(tf.sub(self.y, pred)))
        elif loss_type == 'cross-entropy':
            x_recon = tf.nn.sigmoid(x_recon, name='x_recon')
            # self.gen_loss = -tf.reduce_mean(self.x * tf.log(tf.maximum(x_recon, 1e-10)) 
            #     + (1-self.x)*tf.log(tf.maximum(1-x_recon, 1e-10)))
            self.gen_loss = -tf.reduce_mean(tf.reduce_sum(self.user_x0 * tf.log(tf.maximum(x_recon_user, 1e-10)) 
                + (1-self.user_x0) * tf.log(tf.maximum(1 - x_recon_user, 1e-10)) + 
                self.item_x0 * tf.log(tf.maximum(x_recon_item, 1e-10)) 
                + (1-self.item_x0) * tf.log(tf.maximum(1 - x_recon_item, 1e-10))+ 
                self.y * tf.log(tf.maximum(pred, 1e-10)) 
                + (1-self.y) * tf.log(tf.maximum(1 - pred, 1e-10)),1))

        self.latent_loss = 0.5 * tf.reduce_sum(self.z_log_sigma_sq_user_side - self.z_log_sigma_sq_user - 1 + tf.exp(self.z_log_sigma_sq_user) / tf.exp(self.z_log_sigma_sq_user_side) + tf.pow(self.z_mean_user_side - self.z_mean_user,2) / tf.exp(self.z_log_sigma_sq_user_side) , reduction_indices = 1)
        self.latent_loss += 0.5 * tf.reduce_sum(self.z_log_sigma_sq_item_side - self.z_log_sigma_sq_item - 1 + tf.exp(self.z_log_sigma_sq_item) / tf.exp(self.z_log_sigma_sq_item_side) + tf.pow(self.z_mean_item_side - self.z_mean_item,2) / tf.exp(self.z_log_sigma_sq_item_side) , reduction_indices = 1)
        self.loss = self.gen_loss + self.latent_loss + 2e-4*self.reg_loss
        self.optimizer = tf.train.AdamOptimizer(self.lr).minimize(self.loss)

        # Initializing the tensor flow variables
        init = tf.global_variables_initializer()

        # Launch the session
        self.sess = tf.Session()
        self.sess.run(init)

    # input_dim[user_item,user_info,item_user,item_info]
    def forward(self, user_x0, user_x1, item_x0, item_x1):
        with tf.variable_scope("inference"):
            rec = {'W1_user': tf.get_variable("W1_user", [self.input_dim[0], self.dims[0]], 
                    initializer=tf.contrib.layers.xavier_initializer(), dtype=tf.float32),
                'b1_user': tf.get_variable("b1_user", [self.dims[0]], 
                    initializer=tf.constant_initializer(0.0), dtype=tf.float32),
                'W11_user': tf.get_variable("W11_user", [self.input_dim[1], self.dims[0]], 
                    initializer=tf.contrib.layers.xavier_initializer(), dtype=tf.float32),
                'b11_user': tf.get_variable("b11_user", [self.dims[0]], 
                    initializer=tf.constant_initializer(0.0), dtype=tf.float32),
                'W12_user': tf.get_variable("W12_user", [self.input_dim[1], self.dims[0]], 
                    initializer=tf.contrib.layers.xavier_initializer(), dtype=tf.float32),
                'b12_user': tf.get_variable("b12_user", [self.dims[0]], 
                    initializer=tf.constant_initializer(0.0), dtype=tf.float32),
                'W1_item': tf.get_variable("W1_item", [self.input_dim[2], self.dims[0]], 
                    initializer=tf.contrib.layers.xavier_initializer(), dtype=tf.float32),
                'b1_item': tf.get_variable("b1_item", [self.dims[0]], 
                    initializer=tf.constant_initializer(0.0), dtype=tf.float32),
                'W11_item': tf.get_variable("W11_item", [self.input_dim[3], self.dims[0]], 
                    initializer=tf.contrib.layers.xavier_initializer(), dtype=tf.float32),
                'b11_item': tf.get_variable("b11_item", [self.dims[0]], 
                    initializer=tf.constant_initializer(0.0), dtype=tf.float32),
                'W12_item': tf.get_variable("W12_item", [self.input_dim[3], self.dims[0]], 
                    initializer=tf.contrib.layers.xavier_initializer(), dtype=tf.float32),
                'b12_item': tf.get_variable("b12_item", [self.dims[0]], 
                    initializer=tf.constant_initializer(0.0), dtype=tf.float32),
                'W2_user': tf.get_variable("W2_user", [self.dims[0], self.dims[1]], 
                    initializer=tf.contrib.layers.xavier_initializer(), dtype=tf.float32),
                'b2_user': tf.get_variable("b2_user", [self.dims[1]], 
                    initializer=tf.constant_initializer(0.0), dtype=tf.float32),
                'W2_item': tf.get_variable("W2_item", [self.dims[0], self.dims[1]], 
                    initializer=tf.contrib.layers.xavier_initializer(), dtype=tf.float32),
                'b2_item': tf.get_variable("b2_item", [self.dims[1]], 
                    initializer=tf.constant_initializer(0.0), dtype=tf.float32),
                'W_z_mean_user': tf.get_variable("W_z_mean_user", [self.dims[1], self.n_z], 
                    initializer=tf.contrib.layers.xavier_initializer(), dtype=tf.float32),
                'b_z_mean_user': tf.get_variable("b_z_mean_user", [self.n_z], 
                    initializer=tf.constant_initializer(0.0), dtype=tf.float32),
                'W_z_log_sigma_user': tf.get_variable("W_z_log_sigma_user", [self.dims[1], self.n_z], 
                    initializer=tf.contrib.layers.xavier_initializer(), dtype=tf.float32),
                'b_z_log_sigma_user': tf.get_variable("b_z_log_sigma_user", [self.n_z], 
                    initializer=tf.constant_initializer(0.0), dtype=tf.float32)
                'W_z_mean_item': tf.get_variable("W_z_mean_item", [self.dims[1], self.n_z], 
                    initializer=tf.contrib.layers.xavier_initializer(), dtype=tf.float32),
                'b_z_mean_item': tf.get_variable("b_z_mean_item", [self.n_z], 
                    initializer=tf.constant_initializer(0.0), dtype=tf.float32),
                'W_z_log_sigma_item': tf.get_variable("W_z_log_sigma_item", [self.dims[1], self.n_z], 
                    initializer=tf.contrib.layers.xavier_initializer(), dtype=tf.float32),
                'b_z_log_sigma_item': tf.get_variable("b_z_log_sigma_item", [self.n_z], 
                    initializer=tf.constant_initializer(0.0), dtype=tf.float32),
                'W_z_mean_user_side': tf.get_variable("W_z_mean_user_side", [self.dims[1], self.n_z], 
                    initializer=tf.contrib.layers.xavier_initializer(), dtype=tf.float32),
                'b_z_mean_user_side': tf.get_variable("b_z_mean_user_side", [self.n_z], 
                    initializer=tf.constant_initializer(0.0), dtype=tf.float32),
                'W_z_log_sigma_user_side': tf.get_variable("W_z_log_sigma_user_side", [self.dims[1], self.n_z], 
                    initializer=tf.contrib.layers.xavier_initializer(), dtype=tf.float32),
                'b_z_log_sigma_user_side': tf.get_variable("b_z_log_sigma_user_side", [self.n_z], 
                    initializer=tf.constant_initializer(0.0), dtype=tf.float32)
                'W_z_mean_item_side': tf.get_variable("W_z_mean_item_side", [self.dims[1], self.n_z], 
                    initializer=tf.contrib.layers.xavier_initializer(), dtype=tf.float32),
                'b_z_mean_item_side': tf.get_variable("b_z_mean_item_side", [self.n_z], 
                    initializer=tf.constant_initializer(0.0), dtype=tf.float32),
                'W_z_log_sigma_item_side': tf.get_variable("W_z_log_sigma_item_side", [self.dims[1], self.n_z], 
                    initializer=tf.contrib.layers.xavier_initializer(), dtype=tf.float32),
                'b_z_log_sigma_item_side': tf.get_variable("b_z_log_sigma_item_side", [self.n_z], 
                    initializer=tf.constant_initializer(0.0), dtype=tf.float32)}

        self.reg_loss += tf.nn.l2_loss(rec['W1_user']) + tf.nn.l2_loss(rec['W11_user']) + tf.nn.l2_loss(rec['W1_item']) + tf.nn.l2_loss(rec['W11_item']) + tf.nn.l2_loss(rec['W2_user']) + tf.nn.l2_loss(rec['W2_item'])
        h1_user = self.activate(
            tf.matmul(user_x0, rec['W1_user']) + rec['b1_user'] + tf.matmul(user_x1,rec['W11_user']) + rec['b11_user'], self.activations[0])
        h2_user = self.activate(
            tf.matmul(h1_user, rec['W2_user']) + rec['b2_user'], self.activations[1])
        h1_item = self.activate(
            tf.matmul(item_x0, rec['W1_item']) + rec['b1_item'] + tf.matmul(user_x1,rec['W11_item']) + rec['b11_item'], self.activations[0])
        h2_item = self.activate(
            tf.matmul(h1_user, rec['W2_item']) + rec['b2_item'], self.activations[1])
        h1_user_side = self.activate(
            tf.matmul(user_x1,rec['W12_user']) + rec['b12_user'], self.activations[0])
        h1_item_side = self.activate(
            tf.matmul(item_x1,rec['W12_item']) + rec['b12_item'], self.activations[0])

        self.z_mean_user = tf.matmul(h2_user, rec['W_z_mean_user']) + rec['b_z_mean_user']
        self.z_log_sigma_sq_user = tf.matmul(h2_user, rec['W_z_log_sigma_user']) + rec['b_z_log_sigma_user']

        self.z_mean_item = tf.matmul(h2_item, rec['W_z_mean_item']) + rec['b_z_mean_item']
        self.z_log_sigma_sq_item = tf.matmul(h2_item, rec['W_z_log_sigma_item']) + rec['b_z_log_sigma_item']

        self.z_mean_user_side = tf.matmul(h1_user_size, rec['W_z_mean_user_side ']) + rec['b_z_mean_user_side ']
        self.z_log_sigma_sq_user_side  = tf.matmul(h1_user_size, rec['W_z_log_sigma_user_side ']) + rec['b_z_log_sigma_user_side ']

        self.z_mean_item_side  = tf.matmul(h1_item_side, rec['W_z_mean_item_side ']) + rec['b_z_mean_item_side ']
        self.z_log_sigma_sq_item_side  = tf.matmul(h1_item_side, rec['W_z_log_sigma_item_side ']) + rec['b_z_log_sigma_item_side ']


        eps_user = tf.random_normal((self.params.batch_size, self.n_z), 0, 1, 
            seed=0, dtype=tf.float32)
        self.z_user = self.z_mean_user + tf.sqrt(tf.maximum(tf.exp(self.z_log_sigma_sq_user), 1e-10)) * eps_user

        eps_item = tf.random_normal((self.params.batch_size, self.n_z), 0, 1, 
            seed=0, dtype=tf.float32)
        self.z_item = self.z_mean_item + tf.sqrt(tf.maximum(tf.exp(self.z_log_sigma_sq_item), 1e-10)) * eps_item

        with tf.variable_scope("generation"):
            gen = {'W2_user': tf.get_variable("W2_user", [self.n_z, self.dims[1]], 
                    initializer=tf.contrib.layers.xavier_initializer(), dtype=tf.float32),
                'b2_user': tf.get_variable("b2_user", [self.dims[1]], 
                    initializer=tf.constant_initializer(0.0), dtype=tf.float32),
                'W1_user': tf.transpose(rec['W2_user']),
                'b1_user': rec['b1_user'],
                'W_x_user': tf.transpose(rec['W1_user']),
                'b_x_user': tf.get_variable("b_x_user", [self.input_dim[0]], 
                    initializer=tf.constant_initializer(0.0), dtype=tf.float32)
                'W2_item': tf.get_variable("W2_item", [self.n_z, self.dims[1]], 
                    initializer=tf.contrib.layers.xavier_initializer(), dtype=tf.float32),
                'b2_item': tf.get_variable("b2_item", [self.dims[1]], 
                    initializer=tf.constant_initializer(0.0), dtype=tf.float32),
                'W1_item': tf.transpose(rec['W2_item']),
                'b1_item': rec['b1_item'],
                'W_x_item': tf.transpose(rec['W1_user']),
                'b_x_item': tf.get_variable("b_x_item", [self.input_dim[2]], 
                    initializer=tf.constant_initializer(0.0), dtype=tf.float32),
                'w1_pred_user':tf.get_variable("w1_pred_user", [self.n_z, 100], 
                    initializer=tf.contrib.layers.xavier_initializer(), dtype=tf.float32),
                'w1_pred_item':tf.get_variable("w1_pred_user", [self.n_z, 100], 
                    initializer=tf.contrib.layers.xavier_initializer(), dtype=tf.float32),
                'b1_pred_user': tf.get_variable("b1_pred_user", [100], 
                    initializer=tf.constant_initializer(0.0), dtype=tf.float32),
                'b1_pred_item': tf.get_variable("b1_pred_item", [100], 
                    initializer=tf.constant_initializer(0.0), dtype=tf.float32),
                'w2_pred':tf.get_variable("w2_pred", [100, 1], 
                    initializer=tf.contrib.layers.xavier_initializer(), dtype=tf.float32),
                'b2_pred':tf.get_variable("b1_pred_item", [1], 
                    initializer=tf.constant_initializer(0.0), dtype=tf.float32)
                }

        self.reg_loss += tf.nn.l2_loss(gen['W1_user']) + tf.nn.l2_loss(gen['W_x_user']) + tf.nn.l2_loss(gen['W1_item']) + tf.nn.l2_loss(gen['W_x_item']) + tf.nn.l2_loss(gen['w1_pred_user']) + tf.nn.l2_loss(gen['w1_pred_item']) + tf.nn.l2_loss(gen['w2_pred'])
        h2_user = self.activate(
            tf.matmul(self.z_user, gen['W2_user']) + gen['b2_user'], self.activations[1])
        h1_user = self.activate(
            tf.matmul(h2_user, gen['W1_user']) + gen['b1_user'], self.activations[0])
        x_recon_user = tf.matmul(h1_user, gen['W_x_user']) + gen['b_x_user']
        
        h2_item = self.activate(
            tf.matmul(self.z_item, gen['W2_item']) + gen['b2_item'], self.activations[1])
        h1_item = self.activate(
            tf.matmul(h2_item, gen['W1_item']) + gen['b1_item'], self.activations[0])
        x_recon_item = tf.matmul(h1_item, gen['W_x_item']) + gen['b_x_item']

        h1_pred = self.activate(
            tf.matmul(self.z_user, gen['w1_pred_user']) + gen['b1_pred_user'] + tf.matmul(self.z_item, gen['w1_pred_item']) + gen['b1_item'], self.activations[0])
        pred = self.activate(
            tf.matmul(self.h1_pred, gen['w2_pred']) + gen['b2_pred'],self.activations[1])
        
        return x_recon_user,x_recon_item,pred

    def estimate(self, user_x0, user_x1,item_x0, item_x1,y, num_iter):
        for i in range(num_iter):
            b_x0, ids = utils.get_batch(data_x0, self.params.batch_size)
            b_x1 = data_x1[ids,:]
            _, gen_loss= self.sess.run((self.optimizer, self.gen_loss),
             feed_dict={self.user_x0: user_x0,self.user_x1:user_x1, self.item_x0: item_x0,self.item_x1:item_x1, self.y:y})
            # Display logs per epoch step
            if i % self.print_step == 0 and self.verbose:
                print("Iter:", '%04d' % (i+1), \
                      "genloss=", "{:.5f}".format(gen_loss))
        return gen_loss


    def activate(self, linear, name):
        if name == 'sigmoid':
            return tf.nn.sigmoid(linear, name='encoded')
        elif name == 'softmax':
            return tf.nn.softmax(linear, name='encoded')
        elif name == 'linear':
            return linear
        elif name == 'tanh':
            return tf.nn.tanh(linear, name='encoded')
        elif name == 'relu':
            return tf.nn.relu(linear, name='encoded')

    def run(self, user_x0, user_x1, item_x0, item_x1, y, params):
        loss_list = []
        for epoch in range(params.n_epochs):
            # num_iter = int(n / params.batch_size)
            num_iter = 1
            # gen_loss = self.cdl_estimate(data_x, params.cdl_max_iter)
            gen_loss = self.estimate(user_x0, user_x1, item_x0, item_x1, num_iter)
            loss_list.append(gen_loss)
        loss_list = np.array(loss_list)
        np.savetxt('model/loss_list.txt',loss_list)


